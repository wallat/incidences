﻿using Incidences.CORE;
using Incidences.DAL;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace Incidences.Web
{
    public class Global : HttpApplication
    {
        const string RolAdmin = "Admin";
        const string RolUser = "User";
        const string UserAdmin = "admin@admin.com";
        const string PasswordAdmin = "123Asd@";

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Configuramos seguridad
            ApplicationDbContext context = new ApplicationDbContext();
            RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            //Si no existe el rol Admin, lo crea
            if (!roleManager.RoleExists(RolAdmin))
                roleManager.Create(new IdentityRole(RolAdmin));

            //Si no existe el rol User, lo crea
            if (!roleManager.RoleExists(RolUser))
                roleManager.Create(new IdentityRole(RolUser));

            //Buscamos al usuario Admin, si no está lo creamos
            ApplicationUser user = userManager.FindByName(UserAdmin);
            if(user == null)
            {
                user = new ApplicationUser();
                user.UserName = UserAdmin;
                user.Email = UserAdmin;
                IdentityResult result = userManager.Create(user, PasswordAdmin);
                if(result.Succeeded)
                {
                    userManager.AddToRole(user.Id, RolAdmin);
                }
                else
                {
                    throw new Exception("Usuario no creado");
                }
            }
            else
            {
                //El usuario está creado, ¿Pero ya esta en el rol admin?
                if(!userManager.IsInRole(user.Id, RolAdmin))
                {
                    userManager.AddToRole(user.Id, RolAdmin);
                }
            }

        }
    }
}