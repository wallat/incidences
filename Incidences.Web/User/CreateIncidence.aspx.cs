﻿using Incidences.Application;
using Incidences.CORE;
using Incidences.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using System.Drawing;

namespace Incidences.Web.User
{
    public partial class CreateIncidence : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ddlType.DataSource = null;
                Dictionary<string, string> valuesEnum = new Dictionary<string, string>();
                var values = Enum.GetValues(typeof(IncidenceType));
                foreach (var value in values)
                {
                    valuesEnum.Add(((int)value).ToString(), value.ToString());
                }
                ddlType.DataTextField = "Value";
                ddlType.DataValueField = "Key";
                ddlType.DataSource = valuesEnum;
                ddlType.DataBind();
            }
        }

        protected void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                Incidence incidence = new Incidence()
                {
                    ClosedDate = null,
                    CreatedDate = DateTime.Now,
                    Equipment = txtEquipment.Text,
                    Messages = new List<Message>(),
                    Status = IncidenceStatus.Open,
                    IncidenceType = (IncidenceType)Enum.Parse(typeof(IncidenceType), ddlType.SelectedValue),
                    User_Id = User.Identity.GetUserId()
                };
                Message message = new Message()
                {
                    CreatedDate = DateTime.Now,
                    Incidence = incidence,
                    Text = txtText.Text,
                    User_Id = User.Identity.GetUserId()
                };
                incidence.Messages.Add(message);

                ApplicationDbContext context = new ApplicationDbContext();
                IncidenceManager incidenceManager = new IncidenceManager(context);

                incidenceManager.Add(incidence);
                context.SaveChanges();

                Response.Redirect("ListIncidence");
                //lblResult.Text = "El registro se ha guardado correctamente";
                //lblResult.ForeColor = Color.Green;
                //cmdSave.Enabled = false;
            }
            catch(Exception ex)
            {
                //TODO: Guardar en el log el error
                ErrorMessage.Text = "Se ha producido un error, compruebe que los datos son correctos...";
            }            
        }

        
    }
}