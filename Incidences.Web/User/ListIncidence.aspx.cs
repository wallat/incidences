﻿using Incidences.Application;
using Incidences.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Incidences.Web.Models;

namespace Incidences.Web.User
{
    public partial class ListIncidence : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.UseAccessibleHeader = true;
            if(GridView1.HeaderRow!=null)
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        /// <summary>
        /// Metodo que retorna los datos de las incidencias para el listado de usuario
        /// </summary>
        /// <returns>Lista de incidencias del usuario</returns>
        public List<IncidenceListUser> GetData()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            IncidenceManager incidenceManager = new IncidenceManager(context);
            MessageManager messageMager = new MessageManager(context);

            var incidences = incidenceManager.GetByUserId(User.Identity.GetUserId()).ToList()
                                .Select(i => new IncidenceListUser()
                                {
                                    Id = i.Id,
                                    Equipment = i.Equipment,
                                    Status = i.Status.ToString(),
                                    IncidenceType = i.IncidenceType.ToString(),
                                    Message = messageMager.GetFirstMessage(i.Id).Text
                                });
            return incidences.ToList();
        }

        protected void cmdNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("CreateIncidence");
        }

    }
}