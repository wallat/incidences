﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditIncidence.aspx.cs" Inherits="Incidences.Web.User.EditIncidence" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
         <h2>Incidencias.</h2>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
     <div class="form-horizontal">
        <h4>Modificar incidencia</h4>
        <hr />
         <asp:ValidationSummary  ID="ValidationSummary1" runat="server" CssClass="text-danger" />    
        <div class="form-group">
            <asp:Label ID="lblEquipment" runat="server" Text="Nombre del equipo: "  CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-md-10" style="display:inline-flex">
                <asp:TextBox ID="txtEquipment" runat="server" Width="300px" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="lblType" runat="server" Text="Tipo de incidencia:" CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-md-10" style="display:inline-flex">
                <asp:TextBox ID="txtType" runat="server" Width="300px" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
        </div>        
        <div class="form-group">
            <asp:Label ID="lblStatus" runat="server" Text="Estado de la incidencia:" CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-md-10" style="display:inline-flex">
                <asp:DropDownList ID="ddlStatus" runat="server"  CssClass="form-control" Width="300px">       
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="lblText" runat="server" Text="Texto: "  CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-md-10" style="display:inline-flex">
                <asp:TextBox ID="TxtTexto" runat="server" TextMode="MultiLine" Width="350px" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Debe introducir el texto" ControlToValidate="TxtTexto"  CssClass="text-danger">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <asp:Button ID="cmdSave" runat="server" Text="Guardar" OnClick="btnSave_Click" CssClass="btn btn-default" />
        </div>
        <div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1">
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" />
                    <asp:BoundField DataField="Text" HeaderText="Text" SortExpression="Text" />
                    <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" SortExpression="CreatedDate" />
                    <asp:BoundField DataField="Incidence_Id" HeaderText="Incidence_Id" SortExpression="Incidence_Id" />
                    <asp:BoundField DataField="User_Id" HeaderText="User_Id" SortExpression="User_Id" />
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData" TypeName="Incidences.Web.User.EditIncidence">
                <SelectParameters>
                    <asp:QueryStringParameter Name="incidenceId" QueryStringField="incidenceId" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>

    </div>
    </div>
</asp:Content>
