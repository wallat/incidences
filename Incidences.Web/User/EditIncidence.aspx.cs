﻿using Incidences.Application;
using Incidences.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Incidences.CORE;

namespace Incidences.Web.User
{
    public partial class EditIncidence : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        IncidenceManager incidenceManager = null;
        MessageManager messageManager = null;
        Incidence incidence = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            incidenceManager = new IncidenceManager(context);
            messageManager = new MessageManager(context);

            int id;
            string incidenceId = Request.QueryString["incidenceId"];
            bool parse = int.TryParse(incidenceId, out id);
            if(parse)
            {
                incidence = incidenceManager.GetByIdAndUserId(id, User.Identity.GetUserId());
            }
            if(!parse || incidence==null)
            {
                //Redirigir a una página de error
            }
            if(!Page.IsPostBack)
            {
                ddlStatus.DataSource = null;
                Dictionary<string, string> valuesEnum = new Dictionary<string, string>();
                var values = Enum.GetValues(typeof(IncidenceStatus));
                foreach (var value in values)
                {
                    valuesEnum.Add(((int)value).ToString(), value.ToString());
                }
                ddlStatus.DataTextField = "Value";
                ddlStatus.DataValueField = "Key";
                ddlStatus.DataSource = valuesEnum;
                ddlStatus.DataBind();

                txtEquipment.Text = incidence.Equipment;
                txtType.Text = incidence.IncidenceType.ToString();
            }                
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                incidence.Status = (IncidenceStatus)Enum.Parse(typeof(IncidenceStatus), ddlStatus.SelectedValue);
                messageManager.Add(new Message()
                {
                    CreatedDate = DateTime.Now,
                    Incidence_Id = incidence.Id,
                    Text = TxtTexto.Text,
                    User_Id = User.Identity.GetUserId()
                });
                context.SaveChanges();
                Response.Redirect("ListIncidence");                
            }
            catch(Exception ex)
            {
                //Guardar la excepcion en un log
                ErrorMessage.Text = "Se ha producido un error";
            }
        }

        public List<Message> GetData(int incidenceId)
        {
            context = new ApplicationDbContext();
            messageManager = new MessageManager(context);

            return messageManager.GetAll().Where(m => m.Incidence_Id == incidenceId).OrderBy(m => m.CreatedDate).ToList();
        }
    }
}