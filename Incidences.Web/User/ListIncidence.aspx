﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListIncidence.aspx.cs" Inherits="Incidences.Web.User.ListIncidence" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="<%: ResolveUrl("~/Content/DataTables/css/jquery.dataTables.css") %>" rel="stylesheet" />
    <script src="<%: ResolveUrl("~/Scripts/DataTables/jquery.dataTables.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#MainContent_GridView1').DataTable(
                {
                    "oLanguage": { "sUrl": "//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json" },
                    "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return "<a href='EditIncidence?incidenceId=" + data + "'>" + data + "</a>";
                            },
                            "targets":0
                        },
                        { "title": "Equipo", "targets": 1 },
                        { "title": "Tipo de incidencia", "targets": 2 },
                        { "title": "Mensaje", "targets": 3 },
                        { "title": "Estado", "targets": 4 },
                    ]
                });
        });
    </script>
    <div style="margin-top:10px;margin-bottom:10px">
        <asp:Button CssClass="btn btn-default" ID="cmdNew" runat="server" Text="Nueva Incidencia" OnClick="cmdNew_Click"/> 
    </div>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1">
    <Columns>
        <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" />
        <asp:BoundField DataField="Equipment" HeaderText="Equipment" SortExpression="Equipment" />
        <asp:BoundField DataField="IncidenceType" HeaderText="IncidenceType" SortExpression="IncidenceType" />
        <asp:BoundField DataField="Message" HeaderText="Message" SortExpression="Message" />
        <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
    </Columns>
</asp:GridView>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData" TypeName="Incidences.Web.User.ListIncidence"></asp:ObjectDataSource>
</asp:Content>
