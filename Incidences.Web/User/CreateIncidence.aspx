﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateIncidence.aspx.cs" Inherits="Incidences.Web.User.CreateIncidence" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <h2>Incidencias.</h2>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
     <div class="form-horizontal">
        <h4>Crear una nueva incidencia</h4>
        <hr />
         <asp:ValidationSummary  ID="ValidationSummary1" runat="server" CssClass="text-danger" />    
        <div class="form-group">
            <asp:Label ID="lblEquipment" runat="server" Text="Nombre del equipo: "  CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-md-10" style="display:inline-flex">
                <asp:TextBox ID="txtEquipment" runat="server" Width="300px" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="valEquipment" runat="server" ErrorMessage="Debe introducir el equipo" ControlToValidate="txtEquipment"  CssClass="text-danger">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
        <asp:Label ID="lblText" runat="server" Text="Texto de la incidencia:" CssClass="col-md-2 control-label"></asp:Label>
        <div class="col-md-10" style="display:inline-flex">
            <asp:TextBox ID="txtText" runat="server" TextMode="MultiLine" Width="350px"  CssClass="form-control"></asp:TextBox>
            <asp:RequiredFieldValidator ID="valText" runat="server" ErrorMessage="Debe introducir el texto" ControlToValidate="txtText">*</asp:RequiredFieldValidator>
        </div>
        </div>
        <div class="form-group">
        <asp:Label ID="lblType" runat="server" Text="Tipo de incidencia:" CssClass="col-md-2 control-label"></asp:Label>
        <div class="col-md-10" style="display:inline-flex">
            <asp:DropDownList ID="ddlType" runat="server"  CssClass="form-control" Width="300px">       
            </asp:DropDownList>
        </div>
        </div>
        <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <asp:Button ID="cmdSave" runat="server" Text="Guardar" OnClick="cmdSave_Click" CssClass="btn btn-default" />
        </div>
        </div>
         </div>
</asp:Content>
