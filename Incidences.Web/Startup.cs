﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Incidences.Web.Startup))]
namespace Incidences.Web
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
