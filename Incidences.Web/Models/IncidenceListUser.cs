﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Incidences.Web.Models
{
    /// <summary>
    /// ViewModel del listado de usuario
    /// </summary>
    public class IncidenceListUser
    {
        /// <summary>
        /// Identificador de la incidencia
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Equipo que tienen la incidencia
        /// </summary>
        public string Equipment { get; set; }

        /// <summary>
        /// Tipo de incicencia Hardare o software
        /// </summary>
        public string IncidenceType { get; set; }

        /// <summary>
        /// Lista de mensajes de la incidencia
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Estado de la incidencia
        /// </summary>
        public string Status { get; set; }
    }
}