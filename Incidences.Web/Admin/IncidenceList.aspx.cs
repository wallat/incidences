﻿using Incidences.Application;
using Incidences.CORE;
using Incidences.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;

namespace Incidences.Web.Admin
{
    public partial class IncidenceList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            IncidenceManager incidenceManager = new IncidenceManager(context);
            MessageManager messageManager = new MessageManager(context);

            List<Incidence> list = incidenceManager.GetAll().Include(i=> i.Messages).ToList();

            Incidence incidence = new Incidence();
            incidence.CreatedDate = DateTime.Now;
            incidence.Equipment = "Prueba";
            incidence.IncidenceType = IncidenceType.Software;
            incidence.Priority = Priority.Low;

            Incidence incidence2 = new Incidence()
            {
                 CreatedDate = DateTime.Now,
                 Equipment = "Prueba2",
                 IncidenceType = IncidenceType.Software,
                 Priority = Priority.Low
            };

            incidence = incidenceManager.Add(incidence);
            incidence2 = incidenceManager.Add(incidence2);

            messageManager.Add(new Message()
            {
                 CreatedDate = DateTime.Now,
                 Incidence = incidence,
                 Text = "Prueba de mensaje",
            });

            context.SaveChanges();

            list = incidenceManager.GetAll().ToList();
        }
    }
}