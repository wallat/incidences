﻿using Incidences.CORE;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incidences.DAL
{

    // Enable-Migrations
    // Add-Migration "name xxxxx"
    // Update-Database -StartUpProjectName "Incidences.Web" -ConnectionStringName "DefaultConnection" -Verbose

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        /// <summary>
        /// Colección de incidencias persistibles
        /// </summary>
        public DbSet<Incidence> Incidences { get; set; }

        /// <summary>
        /// Colección de mensages persistibles
        /// </summary>
        public DbSet<Message> Messages { get; set; }
    }
}
