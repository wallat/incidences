namespace Incidences.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteCoste : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Incidences", "Coste");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Incidences", "Coste", c => c.Single(nullable: false));
        }
    }
}
