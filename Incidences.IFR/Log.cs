﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incidences.IFR
{
    public static class Log
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// Método para crear Error en el log
        /// </summary>
        /// <param name="message">texto del mensaje</param>
        /// <param name="ex">excepcion producida</param>
        public static void LogError(string message, Exception ex = null)
        {
            //Escribir en log for net el error
            log.Error(message, ex);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        public static void LogWarn(string message, Exception ex = null)
        {
            //Escribir en log for net el error
            log.Warn(message, ex);
        }
    }
}
