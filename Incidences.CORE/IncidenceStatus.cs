﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Incidences.CORE
{
    /// <summary>
    /// Estado de la incidencia
    /// </summary>
    public enum IncidenceStatus:int
    {
        /// <summary>
        /// Abierta
        /// </summary>
        Open = 0,
        /// <summary>
        /// Cerrada
        /// </summary>
        Close = 1
    }
}
