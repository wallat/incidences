﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Incidences.CORE
{
    /// <summary>
    /// Tipo de incidencia
    /// </summary>
    public enum IncidenceType : int
    {
        /// <summary>
        /// Incidencia de hardware
        /// </summary>
        Hardware = 0,
        /// <summary>
        /// Incidencia de software
        /// </summary>
        Software = 1,
        /// <summary>
        /// Incidencia de otro tipo
        /// </summary>
        Otros = 2
    }
}
