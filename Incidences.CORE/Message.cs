﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Incidences.CORE
{
    /// <summary>
    /// Clase de dominio para mensajes
    /// </summary>
    public class Message
    {
        /// <summary>
        /// Identificador del mensaje
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Testo de la incidencia
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Fecha de creación del mensaje
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Incicidencia a la que pertenece el mensaje
        /// </summary>
        public Incidence Incidence { get; set; }        
        [ForeignKey("Incidence")]
        public int Incidence_Id { get; set; }

        /// <summary>
        /// Usuario que ha creado la inciencia
        /// </summary>
        public ApplicationUser User { get; set; }
        [ForeignKey("User")]
        public string User_Id { get; set; }
    }
}
