﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Incidences.CORE
{
    /// <summary>
    /// Prioridad de la incidencia
    /// </summary>
    public enum Priority : int
    {
        /// <summary>
        /// Prioridad baja
        /// </summary>
        Low = 0,
        /// <summary>
        /// Prioridad media
        /// </summary>
        Medium = 1,
        /// <summary>
        /// Prioridad Alta
        /// </summary>
        High = 2
    }
}
