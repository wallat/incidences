﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incidences.CORE
{
    /// <summary>
    /// Clase de dominio de incidencias
    /// </summary>
    public class Incidence
    {
        /// <summary>
        /// Identificador de la incidencia
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Fecha de creación de la incidencia
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Fecha de cierre de la incidencia
        /// </summary>
        public DateTime? ClosedDate { get; set; }

        /// <summary>
        /// Equipo que tienen la incidencia
        /// </summary>
        public string Equipment { get; set; }

        /// <summary>
        /// Tipo de incicencia Hardare o software
        /// </summary>
        public IncidenceType IncidenceType { get; set; }

        /// <summary>
        /// Lista de mensajes de la incidencia
        /// </summary>
        public List<Message> Messages { get; set; }

        /// <summary>
        /// Usuario que ha creado la inciencia
        /// </summary>
        public ApplicationUser User { get; set; }
        [ForeignKey("User")]
        public string User_Id { get; set; }

        /// <summary>
        /// Prioridad de la incidencia
        /// </summary>
        public Priority Priority { get; set; }

        /// <summary>
        /// Estado de la incidencia
        /// </summary>
        public IncidenceStatus Status { get; set; }

        /// <summary>
        /// Nota interna de la incidencia
        /// </summary>
        public string InternalNote { get; set; }

        /// <summary>
        /// Coste monetario
        /// </summary>
        //public float Coste { get; set; }
    }
}
