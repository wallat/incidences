﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Incidences.MVC.Models
{
    /// <summary>
    /// Clase View Model para crear incidencia
    /// </summary>
    public class IncidenceViewModel
    {
        /// <summary>
        /// Identificador de la incidencia
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Equipo que tiene la incidencia
        /// </summary>
        public string Equipment { get; set; }
        /// <summary>
        /// Texto de la incidencia
        /// </summary>
        public string Text { get; set; }
    }
}