﻿using Incidences.Application;
using Incidences.DAL;
using Incidences.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Incidences.CORE;
using Incidences.IFR;

namespace Incidences.MVC.Controllers
{
    public class IncidenceController : Controller
    {
        //
        // GET: /Incidence/

        public ActionResult Index()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            IncidenceManager incidenceManager = new IncidenceManager(context);
            MessageManager messageManager = new MessageManager(context);
            var incidences = incidenceManager.GetAll().ToList().Select(i=> new IncidenceViewModel()
            {
                 Id = i.Id,
                 Equipment = i.Equipment,
                 Text = messageManager.GetFirstMessage(i.Id).Text
            });
            return View(incidences);
        }

        //
        // GET: /Incidence/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Incidence/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Incidence/Create

        [HttpPost]
        public ActionResult Create(IncidenceViewModel collection)
        {
            try
            {
                // TODO: Add insert logic here
                ApplicationDbContext context = new ApplicationDbContext();
                IncidenceManager incidenceManager = new IncidenceManager(context);

                Incidence inc = new Incidence()
                {
                    Id = collection.Id,
                    Equipment = collection.Equipment,
                    CreatedDate = DateTime.Now,
                    Messages = new List<Message>()
                };
                inc.Messages.Add(new Message()
                {
                        Incidence = inc,
                        Text = collection.Text,
                        CreatedDate = DateTime.Now
                });

                incidenceManager.Add(inc);
                context.SaveChanges();

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                Log.LogError(ex.Message, ex);
                return View();
            }
        }

        //
        // GET: /Incidence/Edit/5

        public ActionResult Edit(int id)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            IncidenceManager incidenceManager = new IncidenceManager(context);
            MessageManager messageManager = new MessageManager(context);
            Incidence incidence = incidenceManager.GetByIdAndUserId(id, null);
            if(incidence != null)
            {
                IncidenceViewModel vm = new IncidenceViewModel()
                {
                    Id = incidence.Id,
                    Equipment = incidence.Equipment,
                    Text = messageManager.GetFirstMessage(incidence.Id).Text
                };
                return View(vm);
            }
            else
            {
                return RedirectToAction("Create");
            }            
        }

        //
        // POST: /Incidence/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, IncidenceViewModel collection)
        {
            try
            {
                // TODO: Add update logic here
                ApplicationDbContext context = new ApplicationDbContext();
                IncidenceManager incidenceManager = new IncidenceManager(context);
                MessageManager messageManager = new MessageManager(context);
                Incidence incidence = incidenceManager.GetByIdAndUserId(id, null);

                incidence.Equipment = collection.Equipment;
                messageManager.Add(new Message()
                {
                    CreatedDate = DateTime.Now,
                    Text = collection.Text,
                    Incidence_Id = id
                });

                context.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Incidence/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Incidence/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
