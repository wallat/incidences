﻿using Incidences.CORE;
using Incidences.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incidences.Application
{
    /// <summary>
    /// Clase para manejar los mensajes
    /// </summary>
    public class MessageManager : Manager<Message>
    {
        /// <summary>
        /// Constructor del manager de mensajes
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public MessageManager(ApplicationDbContext context) : base(context)
        {

        }

        /// <summary>
        /// Retorna el primer mensaje de una incidencia
        /// </summary>
        /// <param name="incidenceId">Identificador de la incidencia</param>
        /// <returns>Mesaje de la incidencia</returns>
        public Message GetFirstMessage(int incidenceId)
        {
            return Context.Messages.Where(m => m.Incidence_Id == incidenceId).OrderBy(i => i.Incidence_Id).FirstOrDefault();
        }
        
    }
}
