﻿using Incidences.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incidences.Application
{
    /// <summary>
    /// Clase generica
    /// </summary>
    public class Manager<T>
        where T :class
    {
        ApplicationDbContext _context = null;

        /// <summary>
        /// Contexto de datos
        /// </summary>
        public ApplicationDbContext Context
        {
            get
            {
                return _context;
            }
        }

        /// <summary>
        /// Constructor del mánager genérico
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public Manager(ApplicationDbContext context)
        {
            _context = context;
        }


        /// <summary>
        /// Método que retorna todos los elementos
        /// </summary>
        /// <returns>Lista de todos los elementos del tipo indicado</returns>
        public IQueryable<T> GetAll()
        {
            return Context.Set<T>();
        }

        
        /// <summary>
        /// Método para añadir un elemento
        /// </summary>
        /// <param name="entity">Entidad a añadir</param>
        /// <returns>Elemento añadido</returns>
        public T Add(T entity)
        {
            return Context.Set<T>().Add(entity);
        }
    }
}
