﻿using Incidences.CORE;
using Incidences.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incidences.Application
{
    /// <summary>
    /// Clase para manejar las incidencias
    /// </summary>
    public class IncidenceManager : Manager<Incidence> 
    {
        /// <summary>
        /// Constructor del manager de incidencias
        /// </summary>
        /// <param name="context"></param>
        public IncidenceManager(ApplicationDbContext context) : base(context)
        {

        }

        /// <summary>
        /// Método que retorna las incidencias de un usuario
        /// </summary>
        /// <param name="userId">Identificador de usuario</param>
        /// <returns>Lista de incidencias</returns>
        public IQueryable<Incidence> GetByUserId(string userId)
        {
            return Context.Incidences.Where(i => i.User_Id == userId);
        }

        /// <summary>
        /// Método que retorna una incidencia por su id y el usuario
        /// </summary>
        /// <param name="id">Identificador de incidencia</param>
        /// <param name="userId">Identificador del usuario</param>
        /// <returns>Incidencia o null en el caso de no existir</returns>
        public Incidence GetByIdAndUserId(int id, string userId)
        {
            return Context.Incidences.Where(i => i.User_Id == userId && i.Id == id).SingleOrDefault();
        }
    }
}
