﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Incidences.DAL;
using Incidences.CORE;

namespace Incidences.Application.Test
{
    [TestClass]
    public class IncidenceManagerTest
    {
        [TestMethod]
        public void GetByUserIdNull()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            IncidenceManager manager = new IncidenceManager(context);

            //Act
            Incidence incidence = manager.GetByIdAndUserId(-1, null);

            //Assert
            Assert.IsNull(incidence);
        }

        [TestMethod]
        public void GetByUserIdFail()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            IncidenceManager manager = new IncidenceManager(context);

            //Act
            Incidence incidence = manager.GetByIdAndUserId(-1, null);

            //Assert
            Assert.IsNotNull(incidence);
        }

        [TestMethod]
        public void GetByUserIdGood()
        {
            //Arrange
            ApplicationDbContext context = new ApplicationDbContext();
            IncidenceManager manager = new IncidenceManager(context);

            //Act
            Incidence incidence = manager.GetByIdAndUserId(1, null);

            //Assert
            Assert.IsNotNull(incidence);
            Assert.AreEqual(incidence.Id, 1);
            Assert.AreEqual(incidence.Equipment, "Equipment1");
            Assert.IsTrue(incidence.Id == 1);
        }
    }
}
